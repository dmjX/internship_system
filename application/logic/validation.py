# See the configure documentation for more about
# this library.
# http://configure.readthedocs.io/en/latest/#
#there is lots of print statements to help you track particular executions
#jus uncomment the ones you want

from configure import Configuration
from application.config import config
from application.models import getModelFromName
from functools import wraps
from flask import request, redirect, url_for, session
import os, re

roleConfig = Configuration.from_file('config/roles.yaml').configure()
from application.models.roleModel import Role

# def getUsernameFromEnv():
#   # FIXME: This is wrong.
#   return os.getenv("USER")
  
#Funtion that gets session user; Added so it can work with the login feature created
def getUsernameFromSession():
  user = session['user']
  return user
  
def getRoles(username):
  roles = []
  for role in roleConfig:
    # print "Checking role '{0}'".format(role)
    if userHasRole (username, role):
      roles.append(role)
  return roles

def doesUserHaveRole (role):
  username = getUsernameFromSession()
  roles = getRoles(username)
  result = (role in roles)
  # print "User '{0}' has roles {1}; returning {2}".format(username, roles, result)
  return result

def userHasRole (username, role):
  # print "Checking role '{0}' for '{1}'".format(role, username)
  # print "There are {0} things in role '{1}'".format(len(roleConfig[role]), roleConfig[role])
  result = False
  
  for ug in roleConfig[role]:
    # print "CHECKING THE UG: {0}".format(ug)
    # We may be referencing another "group," which is a role.
    # Recursively search.
    # print "Handing user/group: '{0}'".format(ug)
    
    # If the ug is an exact match for the username, it means we 
    # have directly coded a username into a group. We should 
    # return True, because we want them to have access. 
    if ug == getUsernameFromSession():
      return True
    
    if re.match('group', ug):
      superRole = re.split(" ", ug)[1]
      # print "Checking a parent group: is {0} in {1}?".format(username, superRole)
      result = userHasRole (username, superRole)
      
    # We may find it is a database lookup.
    elif re.match('database', ug):
      # Get the name of the database
      db = re.split(" ", ug)[1]
      # print "DB is '{0}'".format(db)
      # Get the actual model from the name of the model.
      m = getModelFromName(db)
      # print "Model: {0}".format(m)
      
      # Do a "get", and require that their username and role are both
      # set. For example, look for {jadudm, admin}, not just the username.
      theU = m.get(m.username == username)
      # print ("USERNAME FROM DB: {0} {1}".format(theU.username, theU.role.role))
      # if theU.username == username:
        # print "THE USERNAMES MATCH"
      # if theU.role.role == role:
        # print "THE ROLES MATCH"
      # else:
        # print "THE ROLE FROM THE DB: %s" % theU.role.role
        # print "THE ROLE PASSED IN: %s" % role
        
      # result = m.select().where(m.username == username, Roles.role == role).join(Roles).count()       
      
      
      if (theU.username == username) and (theU.role.role == role):                
        # print "User '{0}' validated via database {1}".format(username, db) 
        return True
      else:
        # print "User '{0}' not found in database {1}".format(username, db)
        result = False
    
    # Check if they are in the Active Directory
    elif re.match('AD', ug):
      # FIXME: Implement this.
      result = False
      
    # If the keyword "ANY" appears, it means anyone 
    # is good to go.
    elif re.match("ANY", ug):
      return True
    
  return result
      
# https://realpython.com/blog/python/primer-on-python-decorators/
# def checkValidUser (fun):
#   @wraps(fun)
#   def wrapper (*args, **kwargs):
#     print "Is Valid User"
#     return fun(*args, **kwargs)
#   return wrapper

def require_role (requiredRole):
  def decorator (fun):
    @wraps(fun)
    def decorated_fun (*args, **kwargs):
      # print "Is Valid Role: %s" % expected_args[0]
      if userHasRole (getUsernameFromSession(), requiredRole):
        print "User has role."
        return fun(*args, **kwargs)
      else:
        print "User does not have role."
        return redirect(url_for(config.application.default), code = 302)
        # return config.application.noRoleHandler
    return decorated_fun
  return decorator
