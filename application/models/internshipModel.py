from application.models.util import *
class Internship (Model):
  iid                       = PrimaryKeyField()
  supervisor_name           = TextField()
  supervisor_phone          = TextField()
  supervisor_email          = TextField()
  title_of_position         = TextField()
  description_of_position   = TextField()
  location_of_internship    = TextField()
  requirements              = TextField()
  class_levels_considered   = TextField()
  majors_considered         = TextField()
  deadline                  = TextField()
  duration_of_internship    = TextField()
  start_date                = TextField()
  hourly_rate               = TextField()
  compensation              = TextField()
  other_stipend             = TextField()
  posted_by                 = TextField()
  is_submitted              = BooleanField(default=False)
  is_approved               = IntegerField(default=0)  
  
  # 0 means pending, 1 means approved and 2 means denied
  

    
  
  class Meta:
    database = getDB("trial")