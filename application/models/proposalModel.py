from application.models.util import *
from application.models.internshipModel import *
from application.models.userrolesModel import Userroles



class Proposal (Model):
  pid                       = PrimaryKeyField()
  iid                       = ForeignKeyField(Internship)
  uaid                      = ForeignKeyField(Userroles)
  Student_BNumber           = TextField() 
  Course                    = TextField() #CSC 395
  Course_Title              = TextField()
  Term                      = TextField() #Summer 2016
  Primary_Sponsor           = ForeignKeyField(Userroles, related_name='primary')
  Secondary_Sponsor         = ForeignKeyField(Userroles, related_name='secondary')
  Academic_Advisor          = ForeignKeyField(Userroles, related_name='academicAdvisor')
  Department_Chair          = ForeignKeyField(Userroles, related_name='deptChair')
  Learning_Objectives       = TextField()
  Preparation_For_Internship = TextField()
  Reflective_Journal        = TextField()
  Final_Paper               = TextField()
  Oral_Presentation         = TextField()
  Optional_Assignments      = TextField()
  Journal_Evaluation        = TextField()
  Final_Paper_Evaluation    = TextField()
  Presentation_Evaluation   = TextField()
  Supervisors_Evaluation    = TextField()
  Optional_Evaluation       = TextField()
  Status                    = TextField()

  class Meta:
    database = getDB("trial")