from application.models.util import *
from application.models.majorsminorsModel import Majorsminors
from application.models.roleModel import Role
from application.models.organizationModel import Organization
from application.models.internshipModel import Internship

class User (Model):
  id                    = PrimaryKeyField()
  oid                   = ForeignKeyField(Organization, null=True)
  iid                   = ForeignKeyField(Internship, null=True)
  role                  = ForeignKeyField(Role)
  dept                  = ForeignKeyField(Majorsminors, null=True)
  active                = BooleanField()
  username              = TextField()
  email                 = TextField()
  first_name            = TextField(null=True)
  last_name             = TextField(null=True)
  phone                 = TextField(null=True)
  title                 = TextField(null=True)
  class_level           = TextField(null=True)
  international_status  = TextField(null=True)
  
  def is_active(self):
    return True
  
  def is_authenticated(self):
    return True
  
  def is_anonymous(self):
    return True
  
  def get_token(self):
    pass
  
  class Meta():
    database = getDB("trial")