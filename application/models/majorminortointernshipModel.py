from application.models.util import *
from application.models.internshipModel import Internship
from application.models.majorsminorsModel import Majorsminors

class Majorminortointernship(Model):
    mmtiid              = PrimaryKeyField()
    Internship          = ForeignKeyField(Internship)
    Majors_Minors       = ForeignKeyField(Majorsminors)
    Non_Berea_Majors    = TextField()
    
    class Meta():
        database = getDB("trial")