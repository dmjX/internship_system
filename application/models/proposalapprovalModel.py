from application.models.util import *
from application.models.userrolesModel import Userroles
from application.models.proposalModel import Proposal

class Proposalapproval(Model):

    paid            = PrimaryKeyField()
    User_Account    = ForeignKeyField(Userroles)
    Proposal        = ForeignKeyField(Proposal)
    
    class Meta():
        database = getDB("trial")