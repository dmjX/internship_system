from application.models.util import *
from application.models.userModel import User

class Token (Model):
  tid                = PrimaryKeyField()
  email              = TextField()
  string             = TextField()              # auto-generated sting 
  timeStamp          = DateTimeField()          # used to set time limit for valid links 

  class Meta:
    database = getDB("trial")