from application.models.util import *
from application.models.majorsminorsModel import Majorsminors
from application.models.userrolesModel import Userroles


class Majorminortouser(Model):
    mmtuid             = PrimaryKeyField()
    Majors_Minors      = ForeignKeyField(Majorsminors)
    User_Account       = ForeignKeyField(Userroles)
    Majors_Or_Minor    = TextField()
    
    class Meta():
        database = getDB("trial")