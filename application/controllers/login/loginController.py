from flask import session,redirect , url_for, render_template, request
from application.logic.validation import require_role
from application.models.userModel import User
from application.models import *
from application.config import *
from application import app



# PURPOSE: To create a session
@app.route('/login', methods = ['GET', 'POST'])
def login():
    def userExists (mail):
      """
      :Purpose: checks the parameter to see if it is equal to 
      :  one of the User emails in the db
      :Input:  an email address
      :Return: returns variable result as True or False 
      """
      result = False
      if User.select().where(User.email == mail).exists():
          result = True
      return result

    if request.method == 'GET':
        return render_template("views/login/loginView.html", config = config)
    data = request.form
    email = data['email']
    if userExists(email):
        print "Is Valid User"
        # print "USER EMAIL AT LOGIN IS : {0}".format(email)
        return redirect(url_for("token", email=email))
    else:
        # print "NO VALID USERS FOR EMAIL: {0}".format(email)
        return redirect(url_for("registration", provided_email=email))
  
