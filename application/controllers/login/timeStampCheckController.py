from application.logic.validation import require_role
from application.models.tokenModel import Token
from application.models.userModel import User
from application.models import *
from application.config import *
from application import app
import datetime
from flask import \
    render_template, \
    session,\
    request, \
    redirect,\
    url_for


def start_session(user, role):
  """
  :Purpose: Create a session
  :Input: a string containing the name you would like to give the session
  :Return: none
  """
  session['user'] = user
  session['role'] = role
  user = session['user']
  # print "THE SESSION NAME IS : {0}".format(user)



def check_token_timeStamp(token):
  """
  :Purpose: Checks the timestamp assosiated with the recieved token to make sure
  :  it is still valid.  If it is, result is returned as "True"
  :Input: a token string
  :Output: True or False depending on if the token both exists and has a valid 
  :  time stamp
  """
  result = False
  valid_token = Token.get(Token.string == token)
  # print "\n\nVALID_TOKEN IS : {0}".format(valid_token)
  # print "CURRENT TIMESTAMP : {0}".format(datetime.datetime.now())
  # print "TOKEN TIMESTAMP : {0}\n".format(valid_token.timeStamp)
  if valid_token.timeStamp > datetime.datetime.now():
    result = True
    # print "(valid_token : {0} ) > (Current Time : {1} )\n\n".format(valid_token.timeStamp, datetime.datetime.now())
  # else:
  #   print "(valid_token : {0} ) < (Current Time : {1} )\n".format(valid_token.timeStamp, datetime.datetime.now())
  return result






# PURPOSE: Checks if token is has a valid time_stamp and starts session.
@app.route('/timeStampCheck/<urlStr>', methods = ['GET'])
def timeStampCheck(urlStr):
  print urlStr
  # Check the timestamp of the recieved token, ['urlStr']
  if check_token_timeStamp(urlStr):
    # get the token object that corresponds with the [''urlStr]
    token = Token.get(Token.string == urlStr)
    # get the user object who's email = that of the token object
    user = User.get(User.email == token.email)
    # set username equal to the username of the user object
    username = user.username
    
    role = user.role.role
    # Create a session named after the User's username
    start_session(username, role) 
    return redirect("/"+role+"Landing")
    # return redirect(url_for("url", urlStr   = urlStr))
  return redirect(url_for("login"))   #!#FixMe:Change the redirect to a registration page #!#
  





