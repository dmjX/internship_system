from application import app
from application.models import *
from application.config import *
from peewee import *
from application.logic.validation import require_role
from application.models.internshipModel import Internship
from wtfpeewee.orm import model_form
from wtforms import Form, BooleanField, StringField, TextAreaField, PasswordField, validators

from flask import \
    render_template, \
    request, \
    url_for

# PURPOSE: Employer edit an existing internship position
@app.route('/editInternshipPosition/<iid>', methods = ['GET', 'POST'])
#@app.route('/editInternshipPosition/<int:entry_id', methods = ['GET', 'POST']) 
#int:entry_id  is the integer id of the particular entry into the db

@require_role('anon')
def editInternshipPosition(iid):
    #try:
    
    entry = Internship.get(Internship.iid==iid) # Entry should be getting a single internship
                                                # for which for which to extract the id and then it can be edited.
                                                # entry_id is explained 

    EntryForm = model_form(Internship)
    if request.method == 'POST':
        form = EntryForm(request.form, obj=entry)
        if form.validate():
            form.populate_obj(entry)
            entry.save()
            flash('Your entry has been saved')
    else:
        form = EntryForm(obj=entry)

    
    return render_template("views/employers/editInternshipPositionView.html",   config = config,
                                                                                navBarConfig = navBarConfig,
                                                                                form = form,
                                                                                entry = entry
                        )

