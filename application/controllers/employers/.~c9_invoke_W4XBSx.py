from application.models.internshipModel import Internship
from application.logic.validation import require_role
from application.models import *
from application.config import *
from application import app
from flask import \
    render_template, \
    request, \
    url_for, \
    session



# PURPOSE: Employer Landing Page
@app.route('/employerInternships', methods = ['GET'])
@require_role('employer')
def employerInternships():
    role = session['role']
     
    print username
    posting = Internship.select()
    return render_template("views/employers/employerInternshipsView.html", config           =         config,
                                                                           navBarConfig     =   navBarConfig,
                                                                           posting          =        posting,
                                                                           role             =           role)

