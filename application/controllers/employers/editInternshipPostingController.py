from wtforms import Form, BooleanField, StringField, TextAreaField, validators
from application.models.internshipModel import Internship
from application.logic.validation import require_role
from wtfpeewee.orm import model_form
from application.models import *
from application.config import *
from application import app
from peewee import *
from flask import \
    render_template, \
    request, \
    url_for, \
    redirect, \
    flash, \
    session



# PURPOSE: Employer edit an existing internship position
@app.route('/editInternshipPosition/<iid>', methods = ['GET', 'POST'])
@require_role('employer')
def editInternshipPosition(iid):
  role = session['role']
  entry = Internship.get(Internship.iid==iid)     #   Entry should be getting a single internship for
                                                  # which to extract the id and then it can be edited.
  EntryForm = model_form(Internship) 
  if request.method == 'POST':
    # Save form to DB 
    form = EntryForm(request.form, obj=entry)
    if form.validate():
        form.populate_obj(entry)
        entry.save()
        flash('Your changes have been saved')
        return redirect(url_for('employerInternships'))
  
  else:
    form = EntryForm(obj=entry)
    
  return render_template("views/employers/editInternshipPositionView.html",   config            =         config,
                                                                              navBarConfig      =   navBarConfig,
                                                                                form            =           form,
                                                                                entry           =          entry,
                                                                                role            =           role)
