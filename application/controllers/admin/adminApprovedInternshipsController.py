from application.models.internshipModel import Internship
from application.logic.validation import require_role
from application.models import *
from application.config import *
from application import app
from flask import \
    render_template, \
    request, \
    session, \
    url_for


# PURPOSE: Approved internships by the admin
@app.route('/adminApprovedInternships', methods = ['GET'])
@require_role('admin')
def adminApprovedInternships():
  # Hard coded to fix issues with cache session
  # Replace with session later
  # Also this role = $_SESSION variables by passes the @require_role
  # And creates a security hole that needs fixing
  role = "admin"
  
  posting = Internship.select().where(Internship.is_approved == True)
  
  return render_template("views/admin/adminApprovedInternshipsView.html", config        = config,
                                                                          navBarConfig  = navBarConfig,
                                                                                   role = role,
                                                                                posting = posting)

