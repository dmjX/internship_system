from application.models.internshipModel import Internship
from application.logic.validation import require_role
from application.models import *
from application.config import *
from application import app
from flask import \
    render_template, \
    request, \
    session, \
    url_for, \
    flash, \
    redirect




# PURPOSE: Admin approve or deny internships
@app.route('/adminApprove/<iid>', methods = ['GET', 'POST'])
@require_role('admin')
def adminApprove(iid):
  print iid
  try:
    internship = Internship.get(Internship.iid == iid)
    internship.is_approved = 1
    internship.save()
    flash("Internship: {} was approved.".format(internship.title_of_position))
    print "Internship: {} was approved.".format(internship.title_of_position)
    redirect(url_for('adminApprovedInternships'))
  except Exception as e:
    print "An error has occured {}".format(e)
    #TODO: log error that is e
    #redirect back to page and say an error occured
    pass
    return render_template("views/admin/adminApproveView.html", config = config)


# PURPOSE: Approved internships by the admin
@app.route('/adminDecline/<iid>', methods = ['GET', 'POST'])
@require_role('admin')
def adminDecline(iid):
  try:
    internship = Internship.get(Internship.iid == iid)
    internship.is_approved = 2
    internship.save()
    print "Internship: {} was declined.".format(internship.title_of_position)
  except Exception as e:
    pass
  return render_template("views/admin/adminDeclineView.html", config = config)

