from application import app
from application.models import *
from application.config import *
from application.logic.validation import require_role

from flask import \
    render_template, \
    request, \
    session, \
    url_for

# PURPOSE: Admin Landing Page
@app.route('/adminLanding', methods = ['GET', 'POST'])
@require_role("admin")
def adminLanding():
  # Hard coded to fix issues with cache session
  # Replace with session later
  # Also this role = $_SESSION variables by passes the @require_role
  # And creates a security hole that needs fixing
  role = "admin"
  return render_template("views/admin/adminLandingView.html", config = config,
                                                            navBarConfig = navBarConfig,
                                                            role = role)
