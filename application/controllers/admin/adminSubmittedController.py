from application.models.internshipModel import Internship
from application.logic.validation import require_role
from application.models import *
from application.config import *
from application import app
from flask import \
    render_template, \
    request, \
    session, \
    url_for




# PURPOSE: Admin view submitted internships
@app.route('/adminSubmitted', methods = ['GET'])
@require_role('admin')
def adminSubmitted():
  # Hard coded to fix issues with cache session
  # Replace with session later
  # Also this role = $_SESSION variables by passes the @require_role
  # And creates a security hole that needs fixing
  role = "admin"
  
  posting = Internship.select().where(Internship.is_submitted == True & Internship.is_approved == False)
  
  return render_template("views/admin/adminSubmittedView.html",     config       = config,
                                                                    navBarConfig = navBarConfig,
                                                                    posting      = posting,
                                                                    role         = role)

